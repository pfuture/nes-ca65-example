# NES 6502 ASM code exploration

### I copied Brad Smith's repository to learn about NES coding : https://github.com/bbbradsmith/NES-ca65-example 

## Now i'm putting this online to share my findings with friends.

You can find the original repository here :

* NES: https://github.com/bbbradsmith/NES-ca65-example
* FDS: https://github.com/bbbradsmith/NES-ca65-example/tree/fds

The original project was first distributed at the NESDev forums:
https://forums.nesdev.com/viewtopic.php?t=11151

**pfuture's notes:**
I work under linux, so the compile_example.bat file is not up to date.  I made build.sh to build under linux.  It's also adjusted to produce debug symbols to load in Mesen.

There's options in Mesen's debugger to load debug data automatically.

In the debugger :

* File->Workspace->Auto-load DBG/MLB files
* Code->Show source code as comments

This depends on the CC65 toolchain, though it only uses the ca65 assembler, not the C compiler:
https://cc65.github.io/

Get the emulator here:
https://mesen.ca/

