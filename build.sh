#!/bin/sh
#https://askubuntu.com/questions/350208/what-does-2-dev-null-mean
rm example.o > /dev/null 2>&1
rm example.nes > /dev/null 2>&1
rm example.map.txt > /dev/null 2>&1
rm example.labels.txt > /dev/null 2>&1
#rm example.nes.ram.nl
#rm example.nes.0.nl
#rm example.nes.1.nl
rm example.dbg > /dev/null 2>&1
echo "."
echo "Compiling..."
ca65 example.s -g -o example.o
echo "."
echo "Linking..."
ld65 -o example.nes -C example.cfg example.o -m example.map.txt -Ln example.labels.txt --dbgfile example.dbg
echo "."
echo "Success!"
